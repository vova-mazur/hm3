const fs = require('fs');
const path = require('path');
const Commentator = require('./commentator');

// Facade
module.exports = class Game {
  constructor(io) {
    this.io = io;
    this.duration = Game.WAITING_DURATION;
    this.isRace = false;
    this.serverRoom = 'room1';
    this.roomProgress = this.createRoomProgress();

    this.generateLvl();
    this.setCommentator();
    this.startTimer();
    this.ioConnect();
  }

  static get WAITING_DURATION() { return 20; }
  static get RACE_DURATION() { return 5 * 60; }

  setCommentator() {
    this.commentator = new Commentator(this.io.in(this.serverRoom), Game.RACE_DURATION);

    // proxy
    this.proxy = new Proxy(this, {
      get(obj, key) {
        if(key === 'state') {
          const { duration, isRace, roomProgress, level } = obj;
          return {
            duration,
            isRace,
            map: roomProgress.map,
            length: level.length,
          }
        }
      }
    })
  }

  startTimer() {
    setInterval(() => {
      if (--this.duration === 0) {
        if (this.isRace = !this.isRace) {
          this.startNewLvl();
          this.duration = Game.RACE_DURATION;
        } else {
          this.duration = Game.WAITING_DURATION;
        }
      }
      this.commentator.emit('state', { state: this.proxy['state'] });
      this.io.in(this.serverRoom).emit('timer', { status: this.isRace ? 'game' : 'pause', duration: this.duration });
    }, 1000)
  }

  createRoomProgress() {
    // Proxy
    return new Proxy({ map: new Map() }, {
      set(targetObj, key, value, proxy) {
        targetObj.map.set(key, value);
        targetObj.map = new Map([...targetObj.map.entries()].sort((a, b) => b[1] - a[1]));
        return true;
      }
    });
  }

  updateProgressForPlayer(nickname, progressValue) {
    this.roomProgress[nickname] = progressValue;
    this.io.in(this.serverRoom).emit('progressReload', { 
      map: JSON.stringify(Array.from(this.roomProgress.map)) 
    });
    this.checkForFinish();
  }

  checkForFinish() {
    let isFinish = true;
    this.roomProgress.map.forEach((value, nickanme) => {
      isFinish &= value === this.level.length;
    })
    if(isFinish) {
      this.duration = Game.WAITING_DURATION;
      this.isRace = false;
    }
  }

  startNewLvl() {
    this.generateLvl();
    this.nulifyMap();
  }

  generateLvl() {
    const countOfLevels = fs.readdirSync(path.join(__dirname, 'texts')).length;
    const rndLvlNumber = Math.floor(Math.random() * countOfLevels) + 1;
    const lvlFilename = `text${rndLvlNumber}.txt`;
    this.level = fs.readFileSync(path.join(__dirname, 'texts', lvlFilename), 'utf8');
  }

  nulifyMap() {
    for (const [nickname, value] of this.roomProgress.map) {
      this.updateProgressForPlayer(nickname, 0);
    }
  }

  getLevel() {
    return this.level;
  }

  ioConnect() {
    this.io.on('connection', socket => {

      socket.on('room', ({ nickname }) => {
        let waitPlayerDuration = this.duration + (this.isRace ? Game.WAITING_DURATION : 0);
        const interval = setInterval(() => {
          if (--waitPlayerDuration === 0 || (!this.isRace && this.duration === Game.WAITING_DURATION)) {
            clearInterval(interval);
            socket.join(this.serverRoom);
            this.updateProgressForPlayer(nickname, 0);
          }
          socket.emit('timer', { status: 'wait', duration: waitPlayerDuration });
        }, 1000);
      })

      socket.in(this.serverRoom).on('reload', ({ nickname, value }) => {
        this.updateProgressForPlayer(nickname, value);
      })

      socket.on('close', ({ nickname }) => {
        this.roomProgress.map.delete(nickname);
      })
    });
  }
}
