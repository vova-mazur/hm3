// abstract class 
class Message {
  constructor(data) {
    this.data = data;
    if (new.target === 'Message') {
      throw new TypeError('This is an abstract class');
    }
  }

  getContent() { }
}

class Greeting extends Message {
  getContent() {
    return 'На вулиці зараз трохи пасмурно, але на Львів Арена зараз просто чудова атмосфера: двигуни гарчать, глядачі посміхаються а гонщики ледь помітно нервують та готуюуть своїх залізних конів до заїзду. А коментувати це все дійстово Вам буду я, Ескейп Ентерович і я радий вас вітати зі словами Доброго Вам дня панове!';
  }
}

class StartList extends Message {
  getContent() {
    let nicknames = this.data;
    let str = 'А тим часом список гонщиків: ';
    nicknames.map((el, index) => {
      str += `${el} під номером ${index + 1}, `
    })
    return str;
  }
}

class Every30Sec extends Message {
  constructor(data) {
    super(data);

    this.ordinalDict = {
      '1': 'перший',
      '2': 'другий',
      '3': 'третій',
      '4': 'четвертий',
      '5': 'п\'ятий',
      '6': 'шостий',
      '7': 'сьомий',
      '8': 'восьмий',
      '9': 'дев\'ятий',
      '10': 'десятий',
    };

    this.conjunctionWords = ['зараз', 'за ним йде', 'згодом', 'потім', 'а', 'а', 'а', 'а', 'а', 'а'];
  }

  getContent() {
    const nicknames = this.data;
    
    let str = '';
    nicknames.map((el, index) => {
      str += `${el} ${this.conjunctionWords[index]} ${this.ordinalDict[index + 1]}, `;
    })

    return str;
  }
}

class In30LettersToFinish extends Message {
  getContent() {
    const nicknames = this.data;
    if (nicknames.length === 1) {
      return `До фінішу залишилось зовсім небагато і схоже що першим його може перетнути ${nicknames[0]}. `
    } else if (nicknames.length === 2) {
      return `До фінішу залишилось зовсім небагато і схоже що першим його може перетнути ${nicknames[0]}. Друге місце може залишитись ${nicknames[1]}. Але давайте дочекаємось фінішу.`;
    } else if (nicknames.lenght > 2) {
      return `До фінішу залишилось зовсім небагато і схоже що першим його може перетнути ${nicknames[0]}. Друге місце може залишитись ${nicknames[1]} чи ${nicknames[2]}. Але давайте дочекаємось фінішу.`;
    }
  }
}

class FinishForPlayer extends Message {
  getContent() {
    const nickname = this.data;
    return `Фінішну пряму пересікає ${nickname}`;
  }
}

class Finish extends Message {
  getContent() {
    const nicknames = this.data;

    if (nicknames.length === 1) {
      return `Фінішну пряму пересікає ${nicknames[0]}і фінальний результат: перше місце посідає ${nicknames[0]}`;
    }
    if (nicknames.lenght === 2) {
      return `Фінішну пряму пересікає ${nicknames[1]}і фінальний результат: перше місце посідає ${nicknames[0]}, друге місце посідає ${nicknames[1]}`;
    }
    return `Фінішну пряму пересікає ${nicknames[nicknames.length - 1]}і фінальний результат: перше місце посідає ${nicknames[0]}, друге місце посідає ${nicknames[1]}, третє місце - ${nicknames[2]}`;
  }
}

class Joke extends Message {
  constructor(data) {
    super(data);
    
    this.jokes = [
      'У дівчині завжди борються дві сутности: кішка, яка хоче гуляти сама собі, й собака, якому потрібний хазяїн…',
      'Абсурд стверджувати, що жінки не мають логіки; ви ж не заперечуєте існування атомів лише тому, що ніколи їх не бачили.',
      'Діти вірять у Діда Мороза, а дорослі в те, що якщо більше разів натиснути на кнопку біля переходу, світлофор швидше переключиться. І хто після цього наївний?',
      'Іноді я використовую в речі неіснуючі слова, щоб здаватися більш імпрелентативним.',
    ];
  }

  getContent() {
    return this.jokes[Math.floor(Math.random() * this.jokes.length)];
  }
}

// factory 
module.exports = class MessageFactory {
  createMessage(type, data) {
    let message;
    if (type === 'greeting') {
      message = new Greeting();
    } else if (type === 'startList') {
      message = new StartList(data);
    } else if (type === 'every30Sec') {
      message = new Every30Sec(data);
    } else if (type === 'in30LettersToFinish') {
      message = new In30LettersToFinish(data);
    } else if (type === 'finishForPlayer') {
      message = new FinishForPlayer(data);
    } else if(type === 'finish') {
      message = new Finish(data);
    } else if (type === 'joke') {
      message = new Joke();
    }

    return message.getContent();
  }
}