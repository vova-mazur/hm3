const MessageFactory = require('./factory');

class EventEmitter {
  constructor() {
    this.events = new Map();
  }

  on(name, fn) {
    const event = this.events.get(name);
    if (event) event.add(fn);
    else this.events.set(name, new Set([fn]));
  }

  emit(name, ...args) {
    const event = this.events.get(name);
    if (!event) return;
    for (const fn of event.values()) {
      fn(...args);
    }
  }
}

module.exports = class Commentator extends EventEmitter {
  constructor(io, raceDuration) {
    super();

    this.raceDuration = raceDuration;
    this.messageFactory = new MessageFactory();
    this.leaders = [];
    this.finish = false;
    this.in30lettersToFinish = false;

    this.on('state', ({ state: { duration, isRace, map, length } }) => {
      let message = this.recognizeMessage({ duration, isRace, map, length })
      io.emit('commentator', message);
    })
  }

  recognizeMessage({ duration, isRace, map, length }) {
    if (isRace && map.size > 0) {
      const nicknames = Array.from(map.keys());
      if (duration === this.raceDuration - 1) {
        return this.messageFactory.createMessage('greeting');
      }
      if (duration === this.raceDuration - 10) {
        return this.messageFactory.createMessage('startList', nicknames);
      }

      let count = 0;
      let resultMsg;
      map.forEach((value, nickname) => {
        if (value === length && !this.leaders.includes(nickname) && !this.finish) {
          resultMsg = this.messageFactory.createMessage('finishForPlayer', nickname);
        }
        if (length - value === 30 && !this.in30lettersToFinish) {
          this.in30lettersToFinish = true;
          resultMsg = this.messageFactory.createMessage('in30LettersToFinish', nicknames);
        }
        count += value;
      })
      if (count === (map.size * length) && !this.finish) {
        this.finish = true;
        return this.messageFactory.createMessage('finish', nicknames);
      }
      if (resultMsg) {
        return resultMsg;
      }
      if (duration % 30 === 0) {
        return this.messageFactory.createMessage('every30Sec', nicknames);
      }
    } else if (duration === 18) {
      return this.messageFactory.createMessage('joke');
    }
  }
}