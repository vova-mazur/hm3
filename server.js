const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');
const fs = require('fs');

const Game = require('./game');

require('./passport.config');

server.listen(5000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'login.html'));
});

app.get('/game', (req, res) => {
  res.sendFile(path.join(__dirname, 'game.html'));
});

app.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, 'login.html'));
});

const game = new Game(io);

// protect
app.get('/level', passport.authenticate('jwt', { session: false }), (req, res) => {
  const level = game.getLevel();
  if (level) {
    res.status(200).json({ level });
  } else {
    res.status(400).json({ err: 'error' });
  }
});

app.post('/login', (req, res) => {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.email === userFromReq.email);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'secret', { expiresIn: '24h' });
    res.status(200).json({ auth: true, token, nickname: userInDB.nickname });
  } else {
    res.status(401).json({ auth: false });
  }
});
